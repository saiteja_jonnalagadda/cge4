/*
*Unit 8
*Chester Condray, Elicia Reuscher
*/

process.env.NODE_ENV = 'test'
const app = require('../../app.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
var request = require('supertest')
const EMAIL = 'test@test.com'
const PASSWORD = 'Password_1'
const path = 'estimate'

LOG.debug('Starting test/controllers/estimateTest.js.')

// set up user credentials to pass to the login method
const userCredentials = {
  email: EMAIL,
  password: PASSWORD
}

// login the user before running tests
var authenticatedUser = request.agent(app)

mocha.describe('API Tests - Estimate Controller - Not Authenticated', function () {
  mocha.describe('GET /estimate', function () {
    mocha.it('responds with status 302 (redirect) when not authenticated', function (done) {
      request(app)
        .get('/' + path)
        .end(function (err, res) {
          if (err) return done(err)
          expect(res.status).to.be.equal(302)
          done()
        })
    })
  })
  mocha.describe('GET /estimate/xyz', function () {
    mocha.it('responds with status 404', function (done) {
      request(app)
        .get('/' + path + '/xyz')
        .end(function (err, res) {
          if (err) { }
          expect(res.status).to.be.equal(404)
          done()
        })
    })
  })
})
mocha.describe('API Tests - Estimate Controller - Authenticated', function () {
  //Login----mocha.before(----Unit 05
  
  mocha.describe('GET /estimate', function () {

    //200----mocha.it('responds with status 200...'----Unit 06

    //Index----mocha.it('should return index page'----Unit 07
    
  })
  mocha.describe('GET /estimate/findall', function () {

    //200----mocha.it('responds with status 200...'----Unit 08

    //Return JSON----mocha.it('should return json'----Unit 09
    
  })
  mocha.describe('GET /puppy/findone/1', function () {

    //200----mocha.it('responds with status 200...'----Unit 10

    //Return JSON----mocha.it('should return json'----Unit 11
    
  })
})