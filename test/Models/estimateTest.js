process.env.NODE_ENV = 'test'
const app = require('../../app.js')
var estimate = require('../../models/estimate.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
const find = require('lodash.find')
const remove = require('lodash.remove')
const ID = 9999999
const NAME = 'Eddie'
const location = 'lab'
const squareFeet = 2
const NEGATIVE_squareFeet = -2
const NON_INT_squareFeet = 2.5

LOG.debug('Starting test/model/estimateTest.js.')

mocha.describe('API Tests - estimate Model', function () {
  mocha.before(function (done) {
    const data = app.locals.estimates.query
    LOG.debug('Before the test, there are %s estimates', data.length)
    const item = new estimate({
      _id: ID,
      name: NAME,
      location: location
    })
    data.push(item)
    LOG.debug('Before the test, we added another for a total of %s estimates', data.length)
    done()
  })
  mocha.after(function (done) {
    const data = app.locals.estimates.query
    LOG.debug('After the test, there are %s estimates', data.length)
    for (let i = 0; i < data.length; i++) {
      LOG.debug(data[i]._id)
    }
    const item = find(data, { '_id': ID })
    if (!item) {
      LOG.error('The item added during the test was not found. _id = ', ID)
    }
    const deletedItem = remove(data, { '_id': ID })
    LOG.info(`Permanently deleted item ${JSON.stringify(deletedItem)}`)
    LOG.debug('After test cleanup, there are %s estimates', data.length)
    done()
  })
  mocha.it('new item should not be null', function (done) {
    const data = app.locals.estimates.query
    const item = find(data, { '_id': ID })
    expect(item).to.not.equal(null)
    done()
  })
  mocha.it('it should have an _id', function (done) {
    const data = app.locals.estimates.query
    const item = find(data, { '_id': ID })
    expect(item._id).to.not.equal(null)
    LOG.debug('item._id = ' + item._id)
    done()
  })
  mocha.it('it should have the given location', function (done) {
    const data = app.locals.estimates.query
    const item = find(data, { '_id': ID })
    expect(item.location).to.equal(location)
    LOG.debug('estimate.location = ' + item.location)
    done()
  })
})

mocha.describe('API Tests - estimate Model Invalid', function () {
  mocha.it('it should be invalid if _id is empty', function (done) {
    var testItem = new estimate({
      location: location,
      squareFeet: squareFeet
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
  mocha.it('it should be invalid if name is empty', function (done) {
    var testItem = new estimate({
      location: location
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
  mocha.it('it should be invalid if squareFeet is negative', function (done) {
    var testItem = new estimate({
      name: NAME,
      location: location,
      squareFeet: NEGATIVE_squareFeet
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
  mocha.it('it should be invalid if squareFeet is not an integer', function (done) {
    var testItem = new estimate({
      name: NAME,
      location: location,
      squareFeet: NON_INT_squareFeet
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
})