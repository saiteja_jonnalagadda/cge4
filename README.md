# C&G Coatings Job Cost Estimator App

# Summary:
This application performs cost estimations for construction and repair jobs. It was designed for C&G Coatings, a General Contractor that specializes in metal roof restoration and cement repair systems. This app was created as a group project to meet requirements of CSIS 44563, Developing Web Applications and Services, at Northwest Missouri State University.

# Developers:
This app was created using Visual Studio Code by Section 4 of CSIS 44563, Developing Web Applications and Services, at Northwest Missouri State University during the Fall Semester of 2017. It was created under the direction of Dr. Denise Case, DCASE@nwmissouri.edu. Details of each contributor can be found under the About page.

# Client:
C&G Coatings
4828 W 157th Street
Overland Park, KS 66224
cgcoatings@kcnet.com - email
Project POC: Mr. Curt Carpenter, curt.carpenter0@gmail.com

# Requirements:
An updated web browser.

# Using the app:
Download and install Node.js and npm, the package manager for JavaScript.
In Windows, right-click on the project folder and "Open Command Window Here as Administrator".
Run "node app" to start the server.  (Hit CTRL-C to stop.)
Point your browser to `http://localhost:8084`.
Routing:
- Get "/" - display the default page
- Get "/findall" - display all estimates
- Get "//findone/:id" - display an estimate with a specific id
- Get "/details/:id" - display details for an estimate with a specific id
- Get "/estimate/select" - select an estimate with a specific id
- Get "/create" - create a new estimate
- Post "/save" - save an estimate
- Get "/edit/:id" - edit an estimate with a specific id
- Post "/save/:id" - update an estimate with a specific id
- Post "/delete/:id" - delete an estimate with a specific id
- Post "/copyfrom/:id" - copy from an estimate with a specific id
- Post "/new-entry" - submit a new entry (add to an array of entries)
- Get "/estimate/print/:id" - print an estimate with a specific id
            
# Application structure
- app.js - Starting point for the application. Defines the express server, requires routes and models. Loads everything and begins listening for events. 
- config/ - configuration information configuration/environment variables
- controllers/ - logic for handling client requests
- data/ - data loaded each time the application starts
- logs/ - contains server logs
- models/ - schema descriptions for custom data types
- public/ - contains client-side assets with styling and images
- routes/ - route definitions for the API
- views/ - EJS - embedded JavaScript and HTML used to create dynamic pages

# Acknowledgements:

# License:
Copyright (c) 2017 Northwest Missouri State University. 

The Northwest Missouri and C&G logos are copyrighted by their respective organizations.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.